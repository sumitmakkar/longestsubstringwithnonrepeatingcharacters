#include <iostream>
#include <vector>

using namespace std;

class Engine
{
private:
    string      str;
    vector<int> alphaVector;
    vector<int> stringVector;
    string      longestStringWithNoCommonCharacters;
    
    void initializeAlphaVector()
    {
        for(int i = 0 ; i < 26 ; i++)
        {
            alphaVector.push_back(-1);
        }
    }
    
    void initializeStringVector()
    {
        int len = (int)str.length();
        for(int i = 0 ; i < len ; i++)
        {
            stringVector.push_back(-1);
        }
    }
    
    void populateStringVector()
    {
        int len = (int)str.length();
        for(int i = 0 ; i < len ; i++)
        {
            stringVector[i]           = alphaVector[str[i] - 'A'];
            alphaVector[str[i] - 'A'] = i;
        }
    }
    
    //Not in use. Use this for checking string vector.
    void displayStringVector()
    {
        int len = (int)str.length();
        for(int i = 0 ; i < len ; i++)
        {
            cout<<stringVector[i]<<" ";
        }
        cout<<endl;
    }
    
    void fillLongestStrWithNoCmnChars(int start , int len)
    {
        for(int i = 0 ; i < len ; i++)
        {
            longestStringWithNoCommonCharacters += str[start+i];
        }
    }
    
public:
    Engine(string s)
    {
        str = s;
        initializeAlphaVector();
        initializeStringVector();
        longestStringWithNoCommonCharacters = "";
    }
    
    string findLongestSubstringWithNonRepeatingCharacters()
    {
        populateStringVector();
        int fStart = 0;
        int fMaxLen = 1;
        int start = 0;
        int len   = (int)str.length();
        int maxLen = 1;
        for(int i = 1 ; i < len ; i++)
        {
            if(stringVector[i] == -1  || (stringVector[i] > -1 && stringVector[i] < start))
            {
                maxLen = i - start + 1;
                if(maxLen > fMaxLen)
                {
                    fStart  = start;
                    fMaxLen = maxLen;
                }
            }
            else
            {
                start = stringVector[i] + 1;
            }
        }
        fillLongestStrWithNoCmnChars(fStart , fMaxLen);
        return longestStringWithNoCommonCharacters;
    }
};

int main(int argc, const char * argv[])
{
    string str = "ABCDABDEFGCABD";
    Engine e   = Engine(str);
    cout<<e.findLongestSubstringWithNonRepeatingCharacters()<<endl;
    return 0;
}
